package br.ucsal.bes20182.testequalidade.restaurante.business;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20182.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.MesaDao;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MesaDao.class })
public class RestauranteBOUnitarioTest {

	/**
	 * M�todo a ser testado: public static Integer abrirComanda(Integer
	 * numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException. Verificar
	 * se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * Lembre-se de verificar a chamada ao ComandaDao.incluir(comanda).
	 * @throws RegistroNaoEncontrado 
	 * @throws MesaOcupadaException 
	 */
	@Test
	public void abrirComandaMesaLivre() throws RegistroNaoEncontrado, MesaOcupadaException {
		
		PowerMockito.mockStatic(MesaDao.class);
		
		PowerMockito.when(MesaDao.obterPorNumero(3)).thenReturn(new Mesa(3));
		PowerMockito.when(MesaDao.obterPorNumero(5)).thenReturn(new Mesa(5));
		
		Integer numeroMesa = 3;
		
		RestauranteBO.abrirComanda(numeroMesa);
	

	}
}
