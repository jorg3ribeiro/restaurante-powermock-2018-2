package br.ucsal.bes20182.testequalidade.restaurante.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import br.ucsal.bes20182.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.MesaDao;
import br.ucsal.bes20182.testequalidade.restaurante.domain.Comanda;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MesaDao.class, ItemDao.class })
public class ComandaUnitarioTest {

	/**
	 * M�todo a ser testado: private Double calcularTotal(). Verificar o c�lculo
	 * do valor total, com um total de 4 itens.
	 * @throws Exception 
	 */
	@Test
	public void calcularTotal4Itens() throws Exception {
		
		PowerMockito.mockStatic(MesaDao.class);
		PowerMockito.mockStatic(ItemDao.class);
		
		Mesa me = new Mesa(3);
		Item it = new Item("pao", 3.0);
		Item it1 = new Item("queijo", 15.0);
		Item it2 = new Item("presunto", 7.0);
		Item it3 = new Item("manteiga", 10.0);
		
		PowerMockito.when(MesaDao.obterPorNumero(me.getNumero())).thenReturn(me);
		PowerMockito.when(ItemDao.obterPorCodigo(it.getCodigo())).thenReturn(it);
		PowerMockito.when(ItemDao.obterPorCodigo(it1.getCodigo())).thenReturn(it1);
		PowerMockito.when(ItemDao.obterPorCodigo(it2.getCodigo())).thenReturn(it2);
		PowerMockito.when(ItemDao.obterPorCodigo(it3.getCodigo())).thenReturn(it3);
		
		Comanda comanda = new Comanda(me);
		comanda.incluirItem(it, 1);
		comanda.incluirItem(it1, 1);
		comanda.incluirItem(it2, 1);
		comanda.incluirItem(it3, 1);
		
		Whitebox.invokeMethod(Comanda.class, "calcularTotal");

	}


}
